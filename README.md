<h1 align="center">Emacs-Helm</h1>

***
<p align="justify">
  <b>Helm</b> is an Emacs framework for incremental completions and narrowing
  selections. It provides an easy-to-use API for developers wishing to build
  their own Helm applications in Emacs, powerful search tools and dozens of
  already built-in commands providing completion to almost everything.
  It is a must-have for anyone using Emacs as a main work environment.
  Helm has been widely adopted by many Emacs power-users.
  It is available in Melpa and can be easily installed from the Emacs package manager.
</p>

***
<p align="center">
  <a href="https://emacs-helm.github.io/helm/"><b>Homepage</b></a> |
  <a href="https://github.com/emacs-helm/helm/wiki"><b>Helm wiki</b></a> |
  <a href="https://github.com/emacs-helm/helm/wiki/FAQ"><b>FAQ</b></a>
</p>

***

<p align="center">
  Helm in action searching with <a href="https://github.com/ggreer/the_silver_searcher"<b>Grep Ag</b></a>
                                   </p>

![Emacs-helm grep ag](helm-grep-ag-persistent.png)
